
package people;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class ReadIn {
    
    public static void read() throws FileNotFoundException{
        Scanner sc = new Scanner(new File("us-500.csv"));
        String n = sc.nextLine();
        while(sc.hasNext()){
            String line = sc.nextLine();
            String[] s = line.split(",");
            Person p = new Person(s[0],s[1],s[4],s[10]);
            People.addPerson(p);
        }
    }
    
}
