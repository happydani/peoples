
package people;

import java.util.ArrayList;
import java.util.List;

public class FilterCity {
    
    public static List<Person> cityFilter(String city, List<Person> p){
        List<Person> back = new ArrayList<>();
        for (Person person : p) {
            if(person.getCity().equals("Chicago")){
                back.add(person);
            }
        }
        return back;
    }
    
}
